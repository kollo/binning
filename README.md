binning
=======
   (c) 1996-2010 by Markus Hoffmann
   
histogram creator

description
===========

binning  is  a little program to produce histograms from space separated input data
files. Output is a space separated histogram.

<pre>
SYNOPSIS
       binning [ options ] filename

USAGE
       Example of use:

       binning -o output.hist -c 3 input.dat


OUTPUT

       If in verbose mode, binning will output some progress information to stdout.  
       This can be completely switched off with the -q option.


FILE OUTPUT

       The  data files are ascii files containing records, one record per line, with
       blank separated data.

       Each line contains the content of one bin (or is a comment, starting with '#').

       The columns have the format:

       1 -- value of the bin
       2 -- number of entries

       In a comment line there are also the number of  valid  datapoints,  the  number 
       of overflows and unterflows given.


OPTIONS

       By  default,  and  if  no  options are given, binning takes the first column of
       the input file, uses 200 bins and the boundaries are [-0.6:0.6].

       --anfang <value>
              Set starting value for data area.  Default: -0.6

       --ende <value>
              set ending value for data area.  Default: 0.6

       --auto Automatically find data area.  By default, this option is switched off.

       --col <number>, -c <number>
              Use column <number> of the input data file.  The default is column 1.

       -n <number>
              Set the number of bins to create.  The default is 200.

       -h, --help
              print a short help message and exit.

       -o file.dat,--output file.dat
              Place data output to file file.dat.  The default filename is output.dat.

       -q     be more quiet.

       -v     be more verbose.

       --version
              show version, program name and other information.




EXAMPLES

       binning -c 68 plotme.dat --anfang 0.99 --ende 1.001 -n 200 -o hist.dat

	   creates a hiostogram with 200 bins out of data found in plotme.dat coulmn 68.
	   The data area starts at 0.99 and ends at 1.001. The histogram is saved into
	   file hist.dat.



</pre>

       This program is free software;  you  can  redistribute  it and/or modify  it  under
       the  terms  of the GNU General Public License as published  by  the  Free  Software
       Foundation; either  version  2 of the License, or (at your option) any  later  ver‐
       sion.

       This  program is distributed in the hope that  it  will  be useful, but WITHOUT ANY
       WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS   FOR   A
       PARTICULAR PURPOSE.   See  the  GNU  General  Public License for more details.

